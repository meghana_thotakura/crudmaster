<?php

namespace Heptagon\Crudmaster\Requests;

use Illuminate\Foundation\Http\FormRequest;
use DB;

class MasterDataRequst extends FormRequest
{
    protected $validation_error_message;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        
        $masters=DB::table('masters')->where('id',$this->master_id)->first();
        $validations=(array)json_decode($masters->validations);
        $this->validation_error_message=(array)json_decode($masters->validation_error_message);
        
        return $validations;
    }
    public function messages()
    {
    
        return $this->validation_error_message;
    }
}
