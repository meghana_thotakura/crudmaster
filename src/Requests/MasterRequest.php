<?php

namespace Heptagon\Crudmaster\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MasterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            //
            "master_name"=>"required",
            "status"=>"required",
            "columns.*"=>"required",
            "validations.*.validation_field"=>"required",
            "validations.*.validation_field"=>"required",
            "validations.*.validation_name.*"=>"required",
            "validations.*.validation_error_message.*"=>"required",
        ];
    }

    public function messages()
    {
    
        return [
            //
            "master_name.required"=>"Master name is mandatory",
            "status.required"=>"Status is mandatory",
            "columns.*.required"=>"Columns are mandatory",
            "validations.*.validation_field.required"=>"Validation field should not be empty",
            "validations.*.validation_name.*.required"=>"Validation name should not be empty",
            "validations.*.validation_error_message.*.required"=>"Validation error message should not be empty",
            
        ];
    }
}
