<?php

use Illuminate\Support\Facades\Route;
use Heptagon\Crudmaster\Controllers\MasterController;
use Heptagon\Crudmaster\Controllers\MasterDataController;


Route::resource('masters', MasterController::class);
Route::resource('mastersData', MasterDataController::class);

Route::get('mastersData/index/{id}', [MasterDataController::class,'index']);
Route::get('mastersData/create/{id}', [MasterDataController::class,'create']);
