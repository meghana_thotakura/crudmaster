<?php

namespace Heptagon\Crudmaster\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Heptagon\Crudmaster\Requests\MasterDataRequst;
use Heptagon\Crudmaster\Services\MasterDataService;
use Heptagon\Crudmaster\Services\MasterService;
use Redirect;

class MasterDataController
{
    //
    public $masterDataService;
    protected $masterService;
    public function __construct(MasterDataService $masterDataService, MasterService $masterService) {
        $this->masterDataService = $masterDataService;
        $this->masterService = $masterService;
    }
    public function index($id)
    {

        $masters_data=$this->masterDataService->list($id);
        $master=$this->masterService->getDetailsById($id);
        return view('crud::mastersData.index')->with('masters_data',$masters_data)->with('master',$master);
       
    }
    public function create($id){
        $master=$this->masterService->getDetailsById($id);
        return view('crud::mastersData.create')->with('master',$master);
    }
    public function edit($id)
    {

        $masters_data=$this->masterDataService->getDetailsById($id);
        $master=$this->masterService->getDetailsById($masters_data['master_id']);
        return view('crud::mastersData.edit',compact('master','masters_data'));

    }
    public function store(MasterDataRequst $request)
    {
       
            $this->masterDataService->store($request);
    
            return response()->json(['success' => 'MasterData added successfully','url'=>url('mastersData/index/'.$request->master_id)]);
           
       
        
     
        
    }
    public function update(MasterDataRequst $request,$id)
    {
        $this->masterDataService->update($id,$request);
        return response()->json(['success' => 'MasterData Updated successfully','url'=>url('mastersData/index/'.$request->master_id)]);

   
    }
    public function destroy($id)
    {
        $related_master_id=$this->masterDataService->delete($id); 
        return Redirect::to('mastersData/index/'.$related_master_id)->with('success', 'MasterData Deleted successfully');
    }
}
