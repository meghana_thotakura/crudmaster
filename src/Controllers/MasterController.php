<?php

namespace Heptagon\Crudmaster\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Heptagon\Crudmaster\Services\MasterService;
use Redirect;
use Heptagon\Crudmaster\Models\Master;
use Heptagon\Crudmaster\Requests\MasterRequest;

class MasterController 
{
    public $masterService;
    public function __construct(MasterService $masterService) {
        $this->masterService = $masterService;
    }

    public function index()
    { 
        $masters=$this->masterService->list();
        return view('crud::masters.index')->with('masters',$masters);

    }

    public function create()
    {
        return view('crud::masters.create');
    }

    public function edit($id)
    {

        $master=$this->masterService->getDetailsById($id);
        return view('crud::masters.edit',compact('master'));

    }
    public function store(MasterRequest $request)
    {
        if($request->status==null){
            $request->status=0;
        }
        $validation_data=$this->masterService->splitValidations($request['validations']);
        $validations=$validation_data['validations'];
        $validation_error_message=$validation_data['validation_error_message'];
    
        $this->masterService->store($request,$validations,$validation_error_message);
        return response()->json(['success' => 'Master added successfully']);
        
    }

    public function update(MasterRequest $request,$id)
    {
        $validation_data=$this->masterService->splitValidations($request['validations']);
        $validations=$validation_data['validations'];
        $validation_error_message=$validation_data['validation_error_message'];

        $this->masterService->update($id,$request,$validations,$validation_error_message);
        return response()->json(['success' => 'Master updated successfully']);
        
    }

    public function destroy($id)
    {
        $this->masterService->delete($id);

        return Redirect::route('masters.index')->with('success', 'Master Deleted successfully');
    }

}
