<?php

namespace Heptagon\Crudmaster\Services;

use Heptagon\Crudmaster\Models\MasterData;
use Carbon\Carbon;

class MasterDataService {

    public function list($id)
    {
   
        $masters_data=MasterData::where('master_id',$id)->get()->toArray();
        
        for($i=0; $i<count($masters_data); $i++){
           
            $masters_data[$i]['data']=(array)json_decode($masters_data[$i]['data']);
        }
       
        return $masters_data;
    }
    
    public function store($request)
    {
        # code...
        MasterData::insert([
            'master_id'=>$request->master_id,
            'status'=>$request->status,
            'data'=>json_encode($request->data),
            'created_by'=>1,
            'updated_by'=>1,
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
        ]
        );
    }
    public function update($id,$request)
    {
     
        MasterData::where('id',$id)->update([
            'master_id'=>$request->master_id,
            'status'=>$request->status,
            'data'=>json_encode($request->data),
            'updated_by'=>1,
            'updated_at'=>Carbon::now(),
        ]
        );
        
    }
    public function delete($id)
    {
        $master_id=MasterData::where('id',$id)->first()->value('master_id');
        MasterData::where('id',$id)->delete();
        return $master_id;
    }
    public function getDetailsById($id)
    {
        $master_data=MasterData::where('id',$id)->first()->toArray();
        $master_data['data']=(array)json_decode($master_data['data']);
        
        return $master_data;
    }
}