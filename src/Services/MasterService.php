<?php

namespace Heptagon\Crudmaster\Services;

use Heptagon\Crudmaster\Models\Master;
use Heptagon\Crudmaster\Models\MasterData;
use Carbon\Carbon;

class MasterService {

    public function list()
    {
        $masters=Master::get()->toArray();  
        return $masters;
    }

    public function store($request,$validations,$validation_error_message)
    {
       
        Master::insert([
            'master_name'=>$request->master_name,
            'status'=>$request->status,
            'validations'=>json_encode($validations),
            'column_name'=>json_encode($request->columns),
            'validation_error_message'=>json_encode($validation_error_message),
            'models_by_id'=>json_encode(""),
            'created_by'=>1,
            'updated_by'=>1,
            'created_at'=>Carbon::now(),
        ]
        );
    }

    public function update($id,$request,$validations,$validation_error_message){
        Master::where('id',$id)->update([
            'master_name'=>$request->master_name,
            'status'=>$request->status,
            'column_name'=>json_encode($request->columns),
            'validations'=>json_encode($validations),
            'validation_error_message'=>json_encode($validation_error_message),
            'models_by_id'=>json_encode(""),
            'updated_by'=>1,
            'updated_at'=>Carbon::now(),
        ]
        
        );
    }

    public function delete($id)
    {
        Master::where('id',$id)->delete();
        MasterData::where('master_id',$id)->delete();
        
    }

    public function splitValidations($values)
    {
        for($i=0;$i<count($values);$i++){

            $field_name=$values[$i]['validation_field'];
            $field_validation=implode("|",$values[$i]['validation_name']);
            $validations['data.'.$field_name]=$field_validation;

            for($val=0;$val<count($values[$i]['validation_name']);$val++){
                $validation_error_message['data.'.$values[$i]['validation_field'].'.'.$values[$i]['validation_name'][$val]]
                =$values[$i]['validation_error_message'][$val];
            }
        }
        $data['validation_error_message']=$validation_error_message;
        $data['validations']=$validations;
        return $data;
    }

    public function getDetailsById($id)
    {
        $master=Master::where('id',$id)->first()->toArray();
        $master['column_name']=json_decode($master['column_name']);
        $master['validation']=(array) json_decode($master['validations']);
        $master['validation_error_message']=(array) json_decode($master['validation_error_message']);
        $master['validations']=[];
        $i=0;
        foreach($master['validation'] as $key => $value) {
            $master['validations'][$i]["validation_field"]=str_replace("data.","",$key);
            $val_arr=explode("|",$value);
            $master['validations'][$i]["validation_name"]=$val_arr;
            for($j=0;$j<count($val_arr);$j++){
                $master['validations'][$i]["validation_error_message"][$j]=$master['validation_error_message'][$key.".".$val_arr[$j]];
            }

            $i++;
        }
        return $master;
    }
}