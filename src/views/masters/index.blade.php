<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Laravel 8 CRUD Tutorial From Scratch</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
   
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" >
</head>
<body>
<div class="container mt-2">
<div class="row">
<div class="col-lg-12 margin-tb">
<div class="pull-left">
<h2>Laravel 8 CRUD Example Tutorial</h2>
</div>
<div class="pull-right mb-2">
<a class="btn btn-success" href="{{ route('masters.create') }}"> Create masters</a>
</div>
</div>
</div>

<table class="table table-bordered">
<tr>
<th>S.No</th>
<th>Master Name</th>
<th>Master Status</th>
<th width="280px">Action</th>
</tr>
@php($increment=1)

@foreach($masters as $master)
<tr>
<td>{{$increment }}</td>
<td>{{ $master['master_name'] }}</td>
<td>@if($master['status']==1)Active @else InActive @endif</td>
<td>
<form action="{{ route('masters.destroy',$master['id']) }}" method="Post">
<a class="btn btn-primary" href="{{ route('masters.edit',$master['id']) }}">Edit</a>
@csrf
@method('DELETE')
<button type="submit" class="btn btn-danger">Delete</button>
<a class="btn btn-dark" href="{{ url('mastersData/index/'.$master['id']) }}">View</a>
@php($increment=$increment+1)
</form>
</td>
</tr>
@endforeach
</table>
</body>
</html>