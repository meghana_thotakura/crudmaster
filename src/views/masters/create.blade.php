<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<style>
    /* Basic Rules */
.switch input { 
    display:none;
}
.switch {
    display:inline-block;
    width:60px;
    height:30px;
    margin:8px;
    transform:translateY(50%);
    position:relative;
}
/* Style Wired */
.slider {
    position:absolute;
    top:0;
    bottom:0;
    left:0;
    right:0;
    border-radius:30px;
    box-shadow:0 0 0 2px #777, 0 0 4px #777;
    cursor:pointer;
    border:4px solid transparent;
    overflow:hidden;
     transition:.4s;
}
.slider:before {
    position:absolute;
    content:"";
    width:100%;
    height:100%;
    background:#777;
    border-radius:30px;
    transform:translateX(-30px);
    transition:.4s;
}

input:checked + .slider:before {
    transform:translateX(30px);
    background:limeGreen;
}
input:checked + .slider {
    box-shadow:0 0 0 2px limeGreen,0 0 2px limeGreen;
}

/* Style Flat */
.switch.flat .slider {
 box-shadow:none;
}
.switch.flat .slider:before {
  background:#FFF;
}
.switch.flat input:checked + .slider:before {
 background:white;
}
.switch.flat input:checked + .slider {
  background:limeGreen;
}
</style>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" >
</head>
<body>

        
<div class="container mt-2">
<div class="row">
<div class="col-lg-12 margin-tb">
<div class="pull-left mb-2">
<h2>Add Master</h2>
</div>
<div id="suc_msgs" style="display:none" class="alert alert-success text-center">
</div>
<div id="err_msgs" style="display:none;" class="alert alert-danger">
</div>
<div class="text-right">
<a class="btn btn-primary" href="{{ route('masters.index') }}"> Back</a>
</div>
</div>
</div>
@if(session('status'))
<div class="alert alert-success mb-1 mt-1">
{{ session('status') }}
</div>
@endif
<form  id="create-form"  enctype="multipart/form-data">
@csrf
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>Master Name:</strong>
<input type="text" name="master_name" class="form-control" placeholder="Master Name">

</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>Master Status:</strong>
<div>
        <label class="switch">
            <input type="checkbox" onclick="statusVal()" value="1" name="status" checked id="status">
            <span class="slider"></span>
        </label> 
      </div>

</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>Master Columns:</strong>
<a href="javascript:void(0);" onclick="addColumn('validation-1')">Add Master Column</a>
<p id="column_fields" data-col-nums="0"></p>
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
    <strong>Validations:</strong>
    <a href="javascript:void(0);" onclick="addvalidationFields('validation-1')">Add Master Column</a>
    <div id='validationDiv' validation-count="0">
        
    </div>
    
   
</div>

</div>
</div>
<button type="submit" class="btn btn-primary ml-3">Submit</button>
</div>
</form>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    addColumn('validation-1')
    addvalidationFields('validation-1')
    $("#err_msgs").hide();
    const columns_num=0;

});
    var frm = $('#create-form');


frm.submit(function (e) {
    $("#err_msgs").hide();
    e.preventDefault();

    $.ajax({
        type: 'POST',
        url: "{{ route('masters.store') }}",
        data: frm.serialize(),
        success: function (data) {
            $("#err_msgs").hide();
                const node = document.createTextNode(data.success);
                document.getElementById("suc_msgs").appendChild(node);
                $("#suc_msgs").show();
                setTimeout(() => {
                window.location.href = "{{route('masters.index')}}";
            }, "1000")
        },
        error: function (data) {
            console.log(data);
            $("#err_msgs").show();
            $("#err_msgs").empty();
            var errors = $.parseJSON(data.responseText).errors;
            
            var error_messages = new Array();

            (Object.values(errors)).forEach(myFunction);
            function myFunction(item, index) {
                if(!error_messages.includes(item[0])){
                    error_messages.push(item[0]);
                    var x = document.createElement("LI");
                    var t = document.createTextNode(item[0]);
                    x.appendChild(t);
                    document.getElementById("err_msgs").appendChild(x);
                }
            }
            
        },
    });
});

    function statusVal() {
        if(document.getElementById("status").value==1){
            document.getElementById("status").value=0
        }else{
            document.getElementById("status").value=1
        }
        console.log(document.getElementById("status").value)
}
    function addColumn() {
      var col_id= document.getElementById("column_fields").getAttribute("data-col-nums")

    var x = document.createElement("INPUT");
    x.setAttribute("type", "text");
    x.setAttribute("name", "columns["+col_id+"]");
    x.setAttribute("class", "form-control m-2");
    x.setAttribute("placeholder", "Master column "+(parseInt(col_id)+1));
    document.getElementById("column_fields").appendChild(x)
    document.getElementById("column_fields").setAttribute("data-col-nums",parseInt(col_id)+1)
}
    function addvalidationFields() {
        var col_id= document.getElementById("validationDiv").getAttribute("validation-count")
        const div1 = document.createElement("div");
        div1.className="row";
       div1.setAttribute("id","validationBlock"+col_id)
       div1.setAttribute("validationcount",0);

      
        var x = document.createElement("INPUT");
        x.setAttribute("type", "text");
        x.setAttribute("name", "validations["+col_id+"][validation_field]");
        x.setAttribute("class", "form-control col-md-3 m-2");
        x.setAttribute("placeholder", "Validation Field");

        var a = document.createElement('a'); 
        var link = document.createTextNode("Add Validation");
        a.appendChild(link);
        a.setAttribute("class", "col-md-3");
        a.setAttribute('onclick',"addMore("+col_id+",'validation-1');")
        a.setAttribute("id","validationlink"+col_id)
        a.title = "Add Validation";
        a.href = "javascript:void(0);";
        a.className="pt-3"
        // padding-top: 20px;
        document.body.appendChild(a);

        var y = document.createElement("INPUT");
        y.setAttribute("type", "text");
        y.setAttribute("class", "form-control col-md-3 m-2");
        y.setAttribute("name", "validations["+col_id+"][validation_name][0]");
        y.setAttribute("placeholder", "Validation Name");
        y.setAttribute("id","validationName0")
        
        
        var z = document.createElement("INPUT");
        z.setAttribute("type", "text");
        z.setAttribute("class", "form-control col-md-3 m-2");
        z.setAttribute("name", "validations["+col_id+"][validation_error_message][0]");
        z.setAttribute("placeholder", "Validation Error Message");
        z.setAttribute("id","validationErrMsg0")
        
        div1.append(...[x,a,y,z])

        document.getElementById("validationDiv").appendChild(div1)
        document.getElementById("validationDiv").setAttribute("validation-count",parseInt(col_id)+1)
    }
    function addMore(id){
        var col_id= parseInt(document.getElementById("validationBlock"+id).getAttribute("validationcount"))+1
        const div2 = document.createElement("div");
        div2.className="col-md-4 m-2";
        var y = document.createElement("INPUT");
        y.setAttribute("type", "text");
        y.setAttribute("class", "form-control col-md-3 m-2");
        y.setAttribute("name", "validations["+id+"][validation_name]["+col_id+"]");
        y.setAttribute("placeholder", "Validation");
        y.setAttribute("id","validationName"+col_id)
        
        
        var z = document.createElement("INPUT");
        z.setAttribute("type", "text");
        z.setAttribute("class", "form-control col-md-3 m-2");
        z.setAttribute("name", "validations["+id+"][validation_error_message]["+col_id+"]");
        z.setAttribute("placeholder", "Validation Error Message");
        z.setAttribute("id","validationErrMsg"+col_id)

        document.getElementById("validationBlock"+id).append(...[div2,y,z])
        document.getElementById("validationBlock"+id).setAttribute("validationcount",parseInt(col_id)+1)
    }
</script>
</body>
</html>