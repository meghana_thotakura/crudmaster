<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Laravel 8 CRUD Tutorial From Scratch</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
   
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" >
</head>
<body>
<div class="container mt-2">
<div class="row">
<div class="col-lg-12 margin-tb">
<div class="pull-left">

</div>
<div class="pull-right mb-2">
<a class="btn btn-success" href="{{ url('mastersData/create/'.$master['id']) }}"> Create {{ucfirst($master['master_name'])}}</a>
</div>
</div>
</div>
@if ($message = Session::get('success'))
<div class="alert alert-success">
<p>{{ $message }}</p>
</div>
@endif
@php($increment=1)
<table class="table table-bordered">
<tr>
<th>S.No</th>
@foreach($master['column_name'] as $column)
<th>{{$column}}</th>
@endforeach
<th width="280px">masterData status</th>
<th width="280px">Action</th>
</tr>
@foreach($masters_data as $master_data)
<tr>
<td>{{ $increment }}</td>
@foreach($master['column_name'] as $column)
<td>{{ $master_data['data'][$column] }}</td>
@endforeach
<td>@if($master_data['status']==1)Active @else InActive @endif</td>
<td>
<form action="{{ route('mastersData.destroy',$master_data['id']) }}" method="Post">
<a class="btn btn-primary" href="{{ route('mastersData.edit',$master_data['id']) }}">Edit</a>
@csrf
@method('DELETE')
<button type="submit" class="btn btn-danger">Delete</button>
</form>
</td>
</tr>
@php($increment=$increment+1)
@endforeach
</table>
</body>
</html>