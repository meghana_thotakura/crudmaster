<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<style>
    /* Basic Rules */
.switch input { 
    display:none;
}
.switch {
    display:inline-block;
    width:60px;
    height:30px;
    margin:8px;
    transform:translateY(50%);
    position:relative;
}
/* Style Wired */
.slider {
    position:absolute;
    top:0;
    bottom:0;
    left:0;
    right:0;
    border-radius:30px;
    box-shadow:0 0 0 2px #777, 0 0 4px #777;
    cursor:pointer;
    border:4px solid transparent;
    overflow:hidden;
     transition:.4s;
}
.slider:before {
    position:absolute;
    content:"";
    width:100%;
    height:100%;
    background:#777;
    border-radius:30px;
    transform:translateX(-30px);
    transition:.4s;
}

input:checked + .slider:before {
    transform:translateX(30px);
    background:limeGreen;
}
input:checked + .slider {
    box-shadow:0 0 0 2px limeGreen,0 0 2px limeGreen;
}

/* Style Flat */
.switch.flat .slider {
 box-shadow:none;
}
.switch.flat .slider:before {
  background:#FFF;
}
.switch.flat input:checked + .slider:before {
 background:white;
}
.switch.flat input:checked + .slider {
  background:limeGreen;
}
</style>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" >
</head>
<body>

<div class="container mt-2">
<div class="row">
<div class="col-lg-12 margin-tb">
<div class="pull-left mb-2">
<h2>Edit {{ucfirst($master['master_name'])}}</h2>
</div>
<div id="suc_msgs" style="display:none" class="alert alert-success text-center">
</div>
<div id="err_msgs" style="display:none" class="alert alert-danger">
</div>
<div class="text-right">
<a class="btn btn-primary" href="{{ url('mastersData/index/'.$master['id']) }}"> Back</a>
</div>
</div>
</div>
@if(session('status'))
<div class="alert alert-success mb-1 mt-1">
{{ session('status') }}
</div>
@endif
<form  id="edit-form" enctype="multipart/form-data">
@csrf
@method('PUT')
<input type="hidden" name="master_id" id="master_id" value="{{$master['id']}}">
<div class="row">

@foreach($master['column_name'] as $column)
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>{{ucfirst($column)}}:</strong>
<input type="text" name="data[{{$column}}]" value="{{$masters_data['data'][$column]}}" class="form-control" >
</div>
</div>
@endforeach

<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>Master Data Status:</strong>
<div>
        <label class="switch">
            <input type="checkbox" onclick="statusVal()" value="1" name="status" checked id="status">
            <span class="slider"></span>
        </label> 
      </div>

</div>
</div>

<button type="submit" class="btn btn-primary ml-3">Submit</button>
</div>
</form>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

    $("#err_msgs").hide();

});
    var frm = $('#edit-form');


frm.submit(function (e) {

    e.preventDefault();

    $.ajax({
        type: 'PUT',
        url: "{{ route('mastersData.update',$masters_data['id']) }}",
        data: frm.serialize(),
        success: function (data) {
            $("#err_msgs").hide();
                const node = document.createTextNode(data.success);
                document.getElementById("suc_msgs").appendChild(node);
                $("#suc_msgs").show();
                setTimeout(() => {
                window.location.href = data.url;
            }, "1000")
        },
        error: function (data) {
            $("#err_msgs").show();
            $("#err_msgs").empty();
            var errors = $.parseJSON(data.responseText).errors;
            console.log(errors)
            var error_messages = new Array();

            (Object.values(errors)).forEach(myFunction);
            function myFunction(item, index) {
                var x = document.createElement("LI");
                var t = document.createTextNode(item[0]);
                x.appendChild(t);
                document.getElementById("err_msgs").appendChild(x);
            }
            
        },
    });
});
    function statusVal() {
//   const val = document.querySelector('input').value;
        if(document.getElementById("status").value==1){
            document.getElementById("status").value=0
        }else{
            document.getElementById("status").value=1
        }
        console.log(document.getElementById("status").value)
}
</script>
</body>
</html>